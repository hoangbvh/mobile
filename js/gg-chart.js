google.charts.load("visualization","1", {packages:["corechart"]});
google.charts.setOnLoadCallback(drawChart);
google.charts.setOnLoadCallback(drawDonut);
function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Year', 'Sales', 'Expenses'],
    ['2004',  1000,      400],
    ['2005',  1170,      460],
    ['2006',  660,       1120],
    ['2007',  1030,      540]
  ]);

  var options = {
    title: 'Company Performance',
    curveType: 'function',
    legend: { position: 'bottom' },
    theme: {chartArea: {width: '70%', height: '70%'}},
    width: '100%',
    height: '100%'
  };

  var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

  chart.draw(data, options);
}

function drawDonut() {
  // pie chart
  var data = google.visualization.arrayToDataTable([
    ['Effort', 'Amount given',],
    ['luong phoi ban ra',     70],
    ['luong thep ban ra',     30]
  ]);

  var options = {
    title: "Bao cao doanh thu theo mac thep",
    pieHole: 0.5,
    pieSliceTextStyle: {
      color: 'white',
    },
    legend: {position: 'right', textStyle: {color: 'black', fontSize: 11},position: 'right', alignment: 'center'},
    theme: {chartArea: {width: '70%', height: '70%'}},
    width: '100%',
    height: '100%'
    // height: ,'100%'width: 400

  };
  var chart = new google.visualization.PieChart(document.getElementById('donut_single'));
  chart.draw(data, options);
}
