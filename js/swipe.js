$(document).ready(function(){
	function prevent_default(e) {
        e.preventDefault();
    }
    function disable_scroll() {
        $(document).on('touchmove', prevent_default);
    }
    function enable_scroll() {
        $(document).unbind('touchmove', prevent_default)
    }
    var x;
    $('.item-approve-wt')
        .on('touchstart', function(e) {
            $('div.open').css('left', '0px').removeClass('open');
            $(e.currentTarget).addClass('open');
            x = e.originalEvent.targetTouches[0].pageX ;
        })
        .on('touchmove', function(e) {
            var change = e.originalEvent.targetTouches[0].pageX - x;
            console.log(change);
            change = Math.min(Math.max(-200, change), 0);
            y=change;
            e.currentTarget.style.left = change + 'px';
            if (change < -10) disable_scroll()
        })
        .on('touchend', function(e) {
            var left = parseInt(e.currentTarget.style.left);
            console.log(left);
            var new_left;
            if (left < -50) {
                new_left = '-200px';
            } 
             else {
                new_left = '0px';
            }
             $('div.open').css('transition','500ms');
            $(e.currentTarget).animate({left: new_left}, 0);
            enable_scroll();
        });
        $(".check-btn").on('touchend',function(e){
            e.preventDefault();
            $(this).closest('.waiting-approve').slideUp('fast');
            // $(this).closest('.waiting-approve').remove();
        });
    
});