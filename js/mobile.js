var timeload = null;
$(document).ready(function(){
	var max_width = $(document).width();
	$('#menu-left').sideNav({
		menuWidth: 240,
		edge: 'left',
		closeOnClick: false
	});
	$('#menu-right').sideNav({
		menuWidth: 240, 
		edge: 'right',
		closeOnClick: false 
	});

	$(document).on('touchend',function(e) {
    var temp = $(e.target).closest('.form-add-report,.form-add-chart').length;
    if(temp==0){
      	$(".form-add-report").fadeOut('fast');
      	$(".form-add-chart").fadeOut('fast');
    }
    });
	$(".add-report").on('touchend',function(e){
		e.stopPropagation()
		$(".form-add-report").fadeIn('fast');
		$(".form-add-chart").css('d-none');
	});
	$(".add-chart").on('touchend',function(e){
		e.stopPropagation()
		$(".form-add-chart").fadeIn('fast');
	});
	$("#input-search").focus(function () {
	    $(this).closest('.search').find('img').first().addClass('d-none');
	    $(this).closest('.search').find('img').last().removeClass('d-none');
	});
	$("#delete-icon").click(function(){
	    $('#input-search').val("");
	    $(this).addClass('d-none');
	    $(this).prev().removeClass('d-none');
	  });
	$(".ui-content").click(function(){
	    $("#search-icon").removeClass('d-none');
	    $("#delete-icon").addClass('d-none');    
	});

	//swipe approved
	function prevent_default(e) {
        e.preventDefault();
    }
    function disable_scroll() {
        $(document).on('touchmove', prevent_default);
    }
    function enable_scroll() {
        $(document).unbind('touchmove', prevent_default)
    }
    var x;
    $('.item-approve-wt').on('touchstart', function(e) {
            $('div.open').css('left', '0px').removeClass('open');
            $(e.currentTarget).addClass('open');
            x = e.originalEvent.targetTouches[0].pageX ;
        }).on('touchmove', function(e) {
            var change = e.originalEvent.targetTouches[0].pageX - x;
            // console.log(change);
            change = Math.min(Math.max(-200, change), 0);
            y=change;
            e.currentTarget.style.left = change + 'px';
            // if (change < -10) disable_scroll()
        }).on('touchend', function(e) {
            var left = parseInt(e.currentTarget.style.left);
            console.log(left);
            var new_left;
            if (left < -50) {
                new_left = '-200px';
            } 
             else {
                new_left = '0px';
            }
             $('div.open').css('transition','500ms');
            $(e.currentTarget).animate({left: new_left}, 0);
            enable_scroll();
        });
        $(".check-btn").on('touchend',function(e){
            e.preventDefault();
            $(this).closest('.waiting-approve').slideUp('fast');
            // $(this).closest('.waiting-approve').remove();
        });



    	$(".menu-tab-approve ul>li a").click(function(){
			 var href = $(this).attr('href');
			 var x = $(this).closest('.ct').find(href).fadeIn(1000);
		});	
		// $(".main-content").tabs()
		$(".tab-bt a").click(function(){
			var hr = $(this).attr('href');
			// console.log(hr)
			var x = $(this).closest('#page-dashboard').find('.page-content').find(hr).fadeIn(1000);
			// console.log(x)
		});


        // swipe page


        	$('.set-width').css('width',max_width);
			var x,y,n_x,n_y,change_x,change_y,old_x,on_report;
			// console.log(max_width);
			var change=0;
			var tab,k=0;
			var tab_change,tab_prev_ch,tab_next_ch;
			$('.page-content').on('touchstart',function(e){
				tab = $("#swipe-tab").find('.active');
				change_x = 0;
				x = e.originalEvent.targetTouches[0].pageX ;
				y = e.originalEvent.targetTouches[0].pageY ;
				old_x = x;
				old_y = y;
				on_report = $(e.target).closest('.fast-rp-content').length;
			}).on('touchmove',function(e){
				var tab_next,tab_prev;
				if(on_report != 1){
					n_x = e.originalEvent.targetTouches[0].pageX;
					n_y = e.originalEvent.targetTouches[0].pageY;
					change_x = old_x - n_x;
					change_y = old_y - n_y;
					if(change_y < 50 && change_y > -50){
						// console.log(change_y);
						// console.log(change_x)
						k = change + change_x;
						var h = tab.attr('href');
						var tab_active = $(this).find(h);
						if(change_x>0 ){
							// console.log(k)
							var h_next = tab.closest('li').next().find('a').attr('href');
							tab_next = $(this).find(h_next);
							tab_next.css('display','block');
							tab_active.css({'transform':'translateX('+-k+'px)','-webkit-transform':'translateX('+-k+'px)',
								'-o-transform':'translateX('+-k+'px)','-moz-transform':'translateX('+-k+'px)',
								'transition':'0ms','-moz-transition':'0ms','-webkit-transition':'0ms','-o-transition':'0ms'});
							tab_next.css({'transform':'translateX('+-k+'px)','-webkit-transform':'translateX('+-k+'px)',
								'-o-transform':'translateX('+-k+'px)','-moz-transform':'translateX('+-k+'px)',
								'transition':'0ms','-moz-transition':'0ms','-webkit-transition':'0ms','-o-transition':'0ms'});
						}
						else{
							// console.log(k)
							var h_prev = tab.closest('li').prev().find('a').attr('href');
							// console.log(h_prev)
							if(h_prev != null){
								tab_prev = $(this).find(h_prev);
								tab_prev.css('display','block');
								z = k + max_width;
								tab_active.css({'transform':'translateX('+-z+'px)','-webkit-transform':'translateX('+-z+'px)',
								'-o-transform':'translateX('+-z+'px)','-moz-transform':'translateX('+-z+'px)',
								'transition':'0ms','-moz-transition':'0ms','-webkit-transition':'0ms','-o-transition':'0ms'});
								tab_prev.css({'transform':'translateX('+-z+'px)','-webkit-transform':'translateX('+-z+'px)',
								'-o-transform':'translateX('+-z+'px)','-moz-transform':'translateX('+-z+'px)',
								'transition':'0ms','-moz-transition':'0ms','-webkit-transition':'0ms','-o-transition':'0ms'});
								// console.log(z)
							}
							else{
								tab_prev = $(this).find(h_prev);
								tab_active.css({'transform':'translateX('+-k+'px)','-webkit-transform':'translateX('+-k+'px)',
								'-o-transform':'translateX('+-k+'px)','-moz-transform':'translateX('+-k+'px)',
								'transition':'0ms','-moz-transition':'0ms','-webkit-transition':'0ms','-o-transition':'0ms'});
								tab_prev.css({'transform':'translateX('+-k+'px)','-webkit-transform':'translateX('+-k+'px)',
								'-o-transform':'translateX('+-k+'px)','-moz-transform':'translateX('+-k+'px)',
								'transition':'0ms','-moz-transition':'0ms','-webkit-transition':'0ms','-o-transition':'0ms'});
							}
							
						}
					}
					
					
				}
				tab_change = tab_active;
				tab_next_ch = tab_next;
				tab_prev_ch = tab_prev;
			}).on('touchend',function(e){
				change = k;
				var size_indicator = (max_width/5)*4;
				var size = $(".tab-bt .indicator").attr('style');
				var size_left  = size.split(/[\s,';',':',\D]+/);
				// console.log(size_left)
				if(k<max_width/2 && k>0 && on_report != 1){
					tab_change.css({'transform':'translateX(0px)','-webkit-transform':'translateX(0px)',
								'-o-transform':'translateX(0px)','-moz-transform':'translateX(0px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});

					tab_next_ch.css({'transform':'translateX(0px)','-webkit-transform':'translateX(0px)',
								'-o-transform':'translateX(0px)','-moz-transform':'translateX(0px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});
					setTimeout(function(){
						tab_next_ch.removeAttr('style');
						tab_next_ch.css({'display':'none'});
						tab_change.removeAttr('style');
						
					},500);
					k=0;
					change = 0;
				}
				else if(k>max_width/2 && on_report != 1){
					console.log("ok"+size_indicator)

					if(size_left[2] <size_indicator-20){
						k=0;
						change = 0;
						tab_change.css({'transform':'translateX('+-max_width+'px)','-webkit-transform':'translateX('+-max_width+'px)',
								'-o-transform':'translateX('+-max_width+'px)','-moz-transform':'translateX('+-max_width+'px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});
						// tab_change.removeClass('active');
						tab_next_ch.css({'transform':'translateX('+-max_width+'px)','-webkit-transform':'translateX('+-max_width+'px)',
								'-o-transform':'translateX('+-max_width+'px)','-moz-transform':'translateX('+-max_width+'px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});
						// tab_next_ch.addClass('active');
						setTimeout(function(){
							tab_next_ch.css({'transform':'translateX(0px)','-webkit-transform':'translateX(0px)',
								'-o-transform':'translateX(0px)','-moz-transform':'translateX(0px)',
								'transition':'0ms','-moz-transition':'0ms','-webkit-transition':'0ms','-o-transition':'0ms'});
						},500);
						// $(".tab-bt .aa").click();

					}
					else{
						tab_change.css({'transform':'translateX(0px)','-webkit-transform':'translateX(0px)',
								'-o-transform':'translateX(0px)','-moz-transform':'translateX(0px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});
						k=0;
						change = 0;
						return false;
					}
				}
				if(k>-max_width/2 && k<0 && on_report != 1){
					if(size_left[1]<size_indicator-1){

						//lỗi
						tab_prev_ch.css({'transform':'translateX('+-max_width+'px)','-webkit-transform':'translateX('+-max_width+'px)',
								'-o-transform':'translateX('+-max_width+'px)','-moz-transform':'translateX('+-max_width+'px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});
						tab_change.css({'transform':'translateX('+-max_width+'px)','-webkit-transform':'translateX('+-max_width+'px)',
								'-o-transform':'translateX('+-max_width+'px)','-moz-transform':'translateX('+-max_width+'px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});
						setTimeout(function(){
							tab_prev_ch.css('display','none');
							tab_change.css({'transform':'translateX(0px)','-webkit-transform':'translateX(0px)',
								'-o-transform':'translateX(0px)','-moz-transform':'translateX(0px)',
								'transition':'0ms','-moz-transition':'0ms','-webkit-transition':'0ms','-o-transition':'0ms'});
						},500);
						k=0;
						change = 0;
						return false;
					}
					else{
						tab_change.css({'transform':'translateX(0px)','-webkit-transform':'translateX(0px)',
								'-o-transform':'translateX(0px)','-moz-transform':'translateX(0px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});
						setTimeout(function(){
							tab_change.removeAttr('style');
						},500);
						k=0;
						change = 0;
						return false;
					}
				}
				else if(k<-max_width/2 && on_report != 1){
					if(size_left[1]<size_indicator-1){
						k=0;
						change = 0;
						tab_change.css({'transform':'translateX(0px)','-webkit-transform':'translateX(0px)',
								'-o-transform':'translateX(0px)','-moz-transform':'translateX(0px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});
						tab_prev_ch.css({'transform':'translateX(0px)','transition':'500ms'});
					}
					else{
						console.log('k');
						tab_change.css({'transform':'translateX(0px)','-webkit-transform':'translateX(0px)',
								'-o-transform':'translateX(0px)','-moz-transform':'translateX(0px)',
								'transition':'500ms','-moz-transition':'500ms','-webkit-transition':'500ms','-o-transition':'500ms'});
						setTimeout(function(){
							tab_change.removeAttr('style');
						},500);
						k=0;
						change = 0;
						return false;
					}
					
					
				}
				// var left = parseInt(e.currentTarget.style.left);
				
				var href,href_prev,href_next;

				// console.log(size_left[1]);
				// console.log(change_x)
				if( change_x > max_width/2 && size_left[2] < size_indicator && on_report != 1){
					var right = parseInt(size_left[1]) - Math.round(max_width/5);
					var left = parseInt(size_left[2]) + Math.round(max_width/5);
					tab.removeClass('active');
					tab.closest('li').next().find('a').addClass("active");
					href_next = tab.closest('li').next().find('a').attr('href');
					href = tab.closest('li').find('a').attr('href');
					// console.log(href)
					var id_next = $(".page-content").find(href_next).attr('id');
					var id = $(".page-content").find(href).attr('id');
					
					setTimeout(function(){
						$("#" + id).removeAttr('style');
						$("#" + id).css('display','none');
					},400);

					setTimeout(function(){
						$("#" + id_next).removeAttr('style');
						$("#" + id_next).css('display','block');
					},400);
					
					$(".tab-bt .indicator").css({'right':right + 'px','left':left + 'px',
						'transition':'all 0.5s ease 0s','-moz-transition':'all 0.5s ease 0s',
						'-webkit-transition':'all 0.5s ease 0s','-o-transition':'all 0.2s ease 0s'});	
				}		
				else if( change_x < -max_width/2 && size_left[1] < size_indicator && on_report != 1){
					var right = parseInt(size_left[1]) + Math.round(max_width/5);
					var left = parseInt(size_left[2]) - Math.round(max_width/5);
					tab.removeClass('active');
					tab.closest('li').prev().find('a').addClass("active");
					href_prev = tab.closest('li').prev().find('a').attr('href');
					href = tab.closest('li').find('a').attr('href');
					// console.log(href)
					var id_prev = $(".page-content").find(href_prev).attr('id');
					var id = $(".page-content").find(href).attr('id');
					setTimeout(function(){
						$("#" + id).removeAttr('style');
						$("#" + id).css('display','none');
					},400);
					setTimeout(function(){
						$("#" + id_prev).removeAttr('style');
						$("#" + id_prev).css('display','block');
					},400);
					$(".tab-bt .indicator").css({'right':right + 'px','left':left + 'px',
						'transition':'all 0.5s ease 0s','-moz-transition':'all 0.5s ease 0s',
						'-webkit-transition':'all 0.5s ease 0s','-o-transition':'all 0.2s ease 0s'});
				}
				change_x=0;
				// }
			});

});